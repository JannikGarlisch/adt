package entry;

import container.Data;

public class Entry implements Data {
	private String contents; // Attribut vom Typ String
	// Vergleiche dem LOMF-Produktdatentyp: entry(contents:String):Entry;
	// contents ist Projektion (= Attribut)
	
	public Entry(String contents) { // Konstruktor, Erkl�rung folgt!!
		this.contents = contents;
	}
	public String getContents() {
		return this.contents;
	}

}
