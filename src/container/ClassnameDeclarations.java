package container;
/**
 * This class contains constants for used class names 
 */
public class ClassnameDeclarations {
/**
 * Name of stack class
 */
	public static final String StackClassName = "stacks.MyStack";
/**
 * Name of class whose instances shall be used as entries in the stack, 
 * This class must offer a constructor with exactly one String parameter
 */
	public static final String DataClassName = "entry.Entry";  	 
}
