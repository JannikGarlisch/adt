package container;
/**
 * Container objects provide storage facility for Data objects.
 * Only one data object is removable by a single operation call.   
 */
public interface Container{
/**
 * Returns true, if this container is empty
 */
	public boolean isEmpty();
/**
 * Effects: Returns the accessible element of the container and removes it
 * Requires: isEmpty() == false 
 */	
	public Data pop();
/**
 * Adds <d> to this container  
 */	
	public void push(Data d);
}
