package stacks;

import container.Container;
import container.Data;
/**
 * 
 * Stack verwalten Data-Objekt gem�� das Last-In-First-Out-Pinzips
 *
 */
public interface Stack extends Container {
	/**
	 * Liefert true ganu dann, wenn der Stack leer ist
	 */
	public boolean isEmpty();
	/**
	 * Liefert das zuletzt hinzugef�gte Elemnt und l�scht es vom Stack
	 * Es muss die folgende Vorbedingung gelten: this.isEmpty() == false
	 */
	public Data pop();
	/**
	 * F�gt <d> hinzu
	 */
	public void push(Data d);
	/**
	 * Liefert das zuletzt hinzugef�gte Element
	 */
	public Data peek();
}
