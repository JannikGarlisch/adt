package stacks;

import java.util.ArrayList;
import java.util.List;

import container.Data;

public class MyStack implements Stack {
	//private List<Data> elements;
	private List<Data> elements = new ArrayList<Data>();
	@Override
	public boolean isEmpty() {
		//return this.elements.size() == 0;
		return this.elements.isEmpty();
	}

	@Override
	public Data pop() {
		Data temp = this.elements.get(getLastIndex());
		this.elements.remove(getLastIndex());
		return temp;
	}
	
	/**
	 * Liefert den gr��ten verkommenden Index in <elements>
	 * Vorbedingung: this.elemnts.size() > 0
	 */
	private int getLastIndex() {
		return this.elements.size() - 1;
	}

	@Override
	public void push(Data d) {
		elements.add(d);
	}

	@Override
	public Data peek() {
		return this.elements.get(getLastIndex());
	}
	
	@Override
	public String toString() {
		String representation = "";
		for (Data current : this.elements) {
			representation = representation + current.getContents();
		}
		return representation;
	
	}
}
