package stacks;
import static org.junit.Assert.*;

import org.junit.Test;

import container.Data;
import entry.Entry;

public class StackTest {

	@Test
	public void test1() {
		assertEquals(true, new MyStack().isEmpty());
	}
	/**
	 * Testfall, der LIFO-Prinzip �berpr�ft
	 */
	@Test
	public void test2() {
		MyStack myStack = new MyStack();
		Entry entry = new Entry("");
		myStack.push(entry);
		Data result = myStack.pop();
		assertSame(entry, result);
		assertTrue(myStack.isEmpty());
	}
	@Test
	public void test3() {
		MyStack myStack = new MyStack();
		Entry entry1 = new Entry("");
		Entry entry2 = new Entry("");
		myStack.push(entry1);
		myStack.push(entry2);
		Data result = myStack.pop();
		assertSame(entry2, result);
		Data result2 = myStack.pop();
		assertSame(entry1, result2);
	}
	// TODO: Test f�r peek
	
}
